<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<h2>Thank you for your message on <s:property value="nowDate" /></h2>

<p>Your Message was:</p>
<p>
	<s:property value="message" />
</p>

<h2>Messages already saved to DB:</h2>
<ul id="messegaList">
    <s:if test="%{getMessages().isEmpty()}">
        <li>No messages available</li>
    </s:if>
    <s:else>
        <s:iterator value="messages">
            <li><s:property value="message"/></li>
        </s:iterator>
    </s:else>
</ul>