<!DOCTYPE>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BMS :: Seat Layout</title>
       
        <style type="text/css">
			html,body,.container-table{
				height:100%;
			}
			.panel{
				height:100%;
				border-radius:0;
			}
			.panel.panel-default{
				border-color:#1f2533;
				background-color:#f0f0f0;
			}
			.panel .panel-heading{
				background-color:#1f2533;
				border-radius:0;
			}
			.panel h4{
				color:#49b88c;
			}
			.panel h6{
				color:#c5c5c7;
			}			
			.nav li a{
				background-color:#FFFFFF;
				color: #000000;
				border-right:solid 1px #dfe2e6;
			}
			.nav li:last-child a{
				border-right:none;
			}
			.nav li.active a, .nav li:hover a{
				background-color:#49b88c;
				color: #FFFFFF;
			}
			.navbar-nav > li > a{
				line-height: 6px;
			}
			.panel-body form{
				background-color:#FFFFFF;
			}
			@media (max-width: 767px) {				
				.nav.col-xs-6 li a{
					border-bottom:solid 1px #dfe2e6;
				}
				.nav li.active a, .nav li a{
					border-right:none;
				}
				.nav.col-xs-6 li:last-child a{
					border-bottom:none;
				}
			}
			
        </style>
    </head>
    <body>
		<div class="container-fluid container-table">
			<div class="panel panel-default">
				<div class="panel-heading col-md-12">
					<div class="panleDes col-md-5 pull-left">
						<h4>Sample Text Goes Here</h2>
						<h6><span id="">Sample Text Goes Here</span></h6>
						<h6><span id="">Sample Text Goes Here</span></h6>
					</div>
					<div class="pull-left col-md-7 col-xs-12">
						<div class="" id="">
							<h6><span id="">Sample Text Goes Here</span></h6>
						</div>
						<div class="" id="">
							<ul class="nav navbar-nav col-xs-6">
								<li ><a href="#">Link 1</a></li>
								<li ><a href="#">Link 2</a></li>
								<li class="active"><a href="#">Link 3</a></li>
							</ul>
						</div>						
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="panel-body">
					<form class="well-lg">					
						<div>
							CONTENT GOES HERE CONTENT GOES HERE CONTENT GOES HERE </br> CONTENT GOES HERE CONTENT GOES HERE CONTENT GOES HERE </br>
							CONTENT GOES HERE CONTENT GOES HERE CONTENT GOES HERE </br> CONTENT GOES HERE CONTENT GOES HERE CONTENT GOES HERE </br>
						</div>
					</form>
				</div>
			</div>
		</div>
    </body>
</html>
